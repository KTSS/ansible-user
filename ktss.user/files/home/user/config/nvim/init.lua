-- CORE SETTINGS
vim.g.mapleader = " " -- Sets leader key
vim.g.maplocalleader = "\\" -- Sets local leader key

-- Basic setings
local options = {
    -- Tab and indentation
    autoindent = true, --Copy indent from current line when pressing ↵
    expandtab = true, --Convert all tabs typed into spaces
    shiftround = true, --Always indent/outdent to the nearest tabstop
    shiftwidth = 6, --Indent/outdent by six colums
    tabstop = 6, --An indentation level for every six columns

    -- Visual
    background = "dark", --Adjusts the default color groups for that background type
    colorcolumn = "100", --Highlights the Nth column
    conceallevel = 0, --Text is shown normally
    cursorcolumn = true, --Highlight the screen column of the cursor
    cursorline = true, --Highlight the screen line   of the cursor
    foldmethod     = 'expr', --Folding used for the current window.
    foldexpr = "nvim_treesitter#foldexpr()", -- Use treesitter as folding backend
    hlsearch = true, --Highlight all incidences of a search pattern, disable with SPC h
    laststatus = 3, --Whether the last window will have a status line
    lazyredraw = false, --Redraw screen when executing macros
    number = true, --Shows line number
    relativenumber = true, --Shows relative instead of absolute numbers
    ruler = false, --Don't show the line and column number of the cursor position
    showcmd = false, --Don't show (partial) command in the last line of the screen.
    showmode = false, --Don't show Insert/Replace/Visual modes in the the last line.
    signcolumn = "yes", --Always show sign column
    splitbelow = true, --New horizontal windows open at the bottom
    splitright = true, --New vertical   windows open at the right
    termguicolors = true, --Enables 24-bit RGB color in the TUI.
    title = true, --Set title of window as the title of document (if not empty)
    wrap = false, --Don't wrap long lines

    -- Others
    --undodir      = '~/.cache/nvim/undodir',   --Defines the undo directory
    clipboard = "unnamedplus", --Always use clipboard for all operations instead of '+/*'
    completeopt = { "menuone", "noselect" }, --Insert-mode completion options
    encoding = "utf-8", --String  encoding used internally.
    mouse = "", --No mouse usage :D
    smartcase = true, --Override the 'ignorecase' option if the search pattern contains upper case characters.
    smartindent = true, --Do smart autoindenting when starting a new line
    timeoutlen = 200, --Time (ms) to wait for a mapped sequenge to complete
    undofile = true, --Allow undo file after :wq-ing
    updatetime = 200, --Faster completion
    wildmode = "longest,list,full", --Enable autocompletion
    winwidth = 99,
}

for key, value in pairs(options) do
    vim.opt[key] = value
end

vim.opt.formatoptions = vim.opt.formatoptions - "c" - "r" - "o" -- Disables automatic commenting on newline

-- NEOVIDE
local neovideoptions = {
    guifont = "Hasklug Nerd Font:h13", -- Font
    linespace = 6,
}

for key, value in pairs(neovideoptions) do
    vim.opt[key] = value
end

-- MAPPINGS
vim.keymap.set("n", "c", '"_c') -- Using  c  to delete content doesn't add/replace content in  0  or  +  registries

-- Arrows are less efficient, noop them
vim.keymap.set("", "<Up>", "<Nop>")
vim.keymap.set("", "<Down>", "<Nop>")
vim.keymap.set("", "<Left>", "<Nop>")
vim.keymap.set("", "<Right>", "<Nop>")
vim.keymap.set("i", "<Up>", "<Nop>")
vim.keymap.set("i", "<Down>", "<Nop>")
vim.keymap.set("i", "<Left>", "<Nop>")
vim.keymap.set("i", "<Right>", "<Nop>")

-- Moving between buffers
vim.keymap.set("", "<F2>", ":bprevious<CR>")
vim.keymap.set("", "<F3>", ":bnext<CR>")
vim.keymap.set("", "<F4>", ":w<CR>bdelete")

vim.keymap.set("n", "Y", "y$") -- Fix uppercase Y behavior

-- Keeping cursor centered
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")
vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-j>", ":cnext<CR>zzzv")

-- Undo break points
vim.keymap.set("i", ",", ",<c-g>u")
vim.keymap.set("i", ".", ".<c-g>u")
vim.keymap.set("i", "!", "!<c-g>u")
vim.keymap.set("i", "?", "?<c-g>u")
vim.keymap.set("i", "(", "(<c-g>u")
vim.keymap.set("i", ")", ")<c-g>u")
vim.keymap.set("i", "[", "[<c-g>u")
vim.keymap.set("i", "]", "]<c-g>u")
vim.keymap.set("i", "{", "{<c-g>u")
vim.keymap.set("i", "}", "}<c-g>u")

-- Jumplist mutations
vim.cmd([[
        nnoremap <expr> j (v:count > 5 ? "m'" . v:count : "") . 'j'
        nnoremap <expr> k (v:count > 5 ? "m'" . v:count : "") . 'k'
]])

-- Stay in indent mode
vim.keymap.set("v", "<", "<gv")
vim.keymap.set("v", ">", ">gv")

-- Moving text without cluttering muh registars
vim.keymap.set("v", "J", "'>+1<CR>gv=gv")
vim.keymap.set("v", "K", "'<-2<CR>gv=gv")
vim.keymap.set("i", "<C-j>", ".+1<CR>==")
vim.keymap.set("i", "<C-k>", ".-2<CR>==")
vim.keymap.set("n", "<leader>j", ".+1<CR>==")
vim.keymap.set("n", "<leader>k", ".-2<CR>==")

-- Save file as sudo on files that require root permission
--  vim.cmd( [[ cnoremap w!! execute  'silent!  write  !sudo  tee  %  >/dev/null'  <BAR>  edit!]] )

vim.keymap.set("n", "S", ":%s//g<Left><Left>") -- Replace all is aliased to S
vim.keymap.set("v", ".", ":normal .<CR>") -- Perform dot commands over visual blocks

-- Shortcutting split navigation, saving a keypress:
vim.keymap.set("", "<C-h>", "<C-w>h")
vim.keymap.set("", "<C-j>", "<C-w>j")
vim.keymap.set("", "<C-k>", "<C-w>k")
vim.keymap.set("", "<C-l>", "<C-w>l")

-- Quickly move between buffers
vim.keymap.set("n", "<A-l>", ":bnext<CR>")
vim.keymap.set("n", "<A-h>", ":bprevious<CR>")

-- Abbreviations
vim.cmd("cnoreabbrev  Q      q")
vim.cmd("cnoreabbrev  Q!     q!")
vim.cmd("cnoreabbrev  Qall   qall")
vim.cmd("cnoreabbrev  Qall!  qall!")
vim.cmd("cnoreabbrev  W      w")
vim.cmd("cnoreabbrev  W!     w!")
vim.cmd("cnoreabbrev  WQ     wq")
vim.cmd("cnoreabbrev  Wa     wa")
vim.cmd("cnoreabbrev  Wq     wq")
vim.cmd("cnoreabbrev  wQ     wq")

-- Navigating with anchors
vim.keymap.set("i", ",,", '<Esc>/<+.*+><CR>"_cf>')
vim.keymap.set("v", ",,", '<Esc>/<+.*+><CR>"_cf>')
vim.api.nvim_set_keymap("", "<CR><CR>", '<Esc>/<+.*+><Enter>"_cf>', { noremap = false })
vim.keymap.set("i", ",anch", "<++>")

-- AUTOCOMMANDS
require('autocommands.others')
require('autocommands.ansible')
require('autocommands.bib')
require('autocommands.html')
require('autocommands.latex')
require('autocommands.markdown')
require('autocommands.neorg')
require('autocommands.roff')
require('autocommands.shell')
require('autocommands.wiki')
require('autocommands.xml')

-- XML FUNCTION
vim.cmd[[
      function! DoPrettyXML()
            " save the filetype so we can restore it later
            let l:origft = &ft
            set ft=

            " delete the xml header if it exists. This will
            " permit us to surround the document with fake tags
            " without creating invalid xml.
            1s/<?xml .*?>//e

            " insert fake tags around the entire document.
            " This will permit us to pretty-format excerpts of
            " XML that may contain multiple top-level elements.
            0put ='<PrettyXML>'
            $put ='</PrettyXML>'
            silent %!xmllint --format -

            " xmllint will insert an <?xml?> header. it's easy enough to delete
            " if you don't want it.
            " delete the fake tags
            2d
            $d

            " restore the 'normal' indentation, which is one extra level
            " too deep due to the extra tags we wrapped around the document.
            silent %<

            " back to home
            1

            " restore the filetype
            exe "set ft=" . l:origft
      endfunction
      command! PrettyXML call DoPrettyXML()
]]

-- LAZY: Plugin manager
-- Automatically install lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
      vim.fn.system({
            "git",
            "clone",
            "--filter=blob:none",
            "https://github.com/folke/lazy.nvim.git",
            "--branch=stable", -- latest stable release
            lazypath,
      })
end
vim.opt.rtp:prepend(lazypath)
require("lazy").setup("plugins")
