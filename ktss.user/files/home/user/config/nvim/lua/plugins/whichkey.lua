return {
      "folke/which-key.nvim",
      event = "VeryLazy",
      opts = {
            plugins = {
                  marks = true, -- shows a list of your marks on ' and `
                  registers = true, -- shows your registers on " in NORMAL or <C-r> in INSERT mode
                  spelling = true,
                  presets = {
                        operators = true, -- adds help for operators like d, y, ...
                        motions = true, -- adds help for motions
                        text_objects = true, -- help for text objects triggered after entering an operator
                        windows = true, -- default bindings on <c-w>
                        nav = true, -- misc bindings to work with windows
                        z = true, -- bindings for folds, spelling and others prefixed with z
                        g = true, -- bindings for prefixed with g
                  },
            },
            -- add operators that will trigger motion and text object completion
            -- to enable all native operators, set the preset / operators plugin above
            operators = { gc = "Comments" },
            motions = { count = true, },
            icons = {
                  breadcrumb = "󰶻 ", -- symbol used in the command line area that shows your active key combo
                  separator = "➜ ", -- symbol used between a key and it's label
                  group = " ", -- symbol prepended to a group
            },
            popup_mappings = {
                  scroll_down = "<c-d>", -- binding to scroll down inside the popup
                  scroll_up = "<c-u>", -- binding to scroll up inside the popup
            },
            window = {
                  border = "rounded",       -- none, single, double, shadow
                  position = "bottom",      -- bottom, top
                  margin = { 1, 0, 1, 0 },  -- extra window margin [top, right, bottom, left]
                  padding = { 2, 2, 2, 2 }, -- extra window padding [top, right, bottom, left]
                  winblend = 0,
            },
            ignore_missing = false, -- enable this to hide mappings for which you didn't specify a label
            hidden = { "<silent>", "<cmd>", "<Cmd>", "<CR>", "^:", "^ ", "^call ", "^lua " }, -- hide mapping boilerplate
            show_help = true, -- show a help message in the command line for using WhichKey
            show_keys = true, -- show the currently pressed key and its label as a message in the command line
            triggers = "auto", -- automatically setup triggers
            -- triggers = {"<leader>"} -- or specifiy a list manually
            -- list of triggers, where WhichKey should not wait for timeoutlen and show immediately
            triggers_nowait = {
                  -- marks
                  "`",
                  "'",
                  "g`",
                  "g'",
                  -- registers
                  '"',
                  "<c-r>",
                  -- spelling
                  "z=",
            },
            triggers_blacklist = {
                  -- list of mode / prefixes that should never be hooked by WhichKey
                  -- this is mostly relevant for keymaps that start with a native binding
                  i = { "j", "k" },
                  v = { "j", "k" },
            },
            nmappings = {
                  mode = "n",
                  prefix = "<leader>",
                  buffer = nil,   -- Global mappings. Specify a buffer number for buffer local mappings
                  silent = true,  -- use `silent` when creating keymaps
                  noremap = true, -- use `noremap` when creating keymaps
                  nowait = true,  -- use `nowait` when creating keymaps
                        ["/"] = { '<Plug>(comment_toggle_linewise_current)', "Comment/Uncomment" },
                        ["."] = { "<cmd>Lexplore<CR>", "Open NetRW" },
                        b = {
                              name = "Buffers",
                              c = { "<cmd>bdelete<CR>", "Close" }, -- Close
                              d = { "<cmd>bdelete<CR>", "Close" }, -- Delete
                              k = { "<cmd>bdelete<CR>", "Close" }, -- Kill
                              n = { "<cmd>bnext<CR>", "Next" },
                              l = { "<cmd>buffers<CR>", "List" },
                              p = { "<cmd>bprevious<CR>", "Previous" },
                              s = { "<cmd>lua require('telescope.builtin').buffers(require('telescope.themes').get_dropdown{previewer = false})<CR>", "Search" },
                        },
                        ["c"] = { '<cmd>w! <BAR> !compiler "<c-r>%"<CR>', "Compile document" }, -- Groff/LaTeX/MarkDown/etc. <BAR> = "|"
                        ["F"] = { "<cmd>ZenMode<CR>", "Focus" },
                        g = {
                              name = "Git",
                              B = { "<cmd>Telescope git_bcommits<CR>", "Git BCommits" },
                              b = { "<cmd>Telescope git_branches<CR>", "Checkout branch" },
                              c = { "<cmd>Telescope git_commits<CR>", "Checkout commit" },
                              d = { "<cmd>Gitsigns diffthis<CR>", "Diff", },
                              g = {
                                    name = "Neogit",
                                    g = { "<cmd>Neogit<CR>", "New tab" },
                                    s = { "<cmd>Neogit kind=split<CR>", "Split Below" },
                                    a = { "<cmd>Neogit kind=split_above<CR>", "Split Above" },
                                    v = { "<cmd>Neogit kind=vsplit<CR>", "Vertical Split" },
                                    f = { "<cmd>Neogit kind=floating<CR>", "Floating" },

                              },
                              h = { "<cmd>Gitsigns diffthis HEAD<CR>", "Diff this with HEAD"},
                              j = { "<cmd>Gitsigns next_hunk<CR>", "Next Hunk" },
                              k = { "<cmd>Gitsigns prev_hunk<CR>", "Prev Hunk" },
                              l = { "<cmd>Gitsigns blame_line<CR>", "Blame" },
                              o = { "<cmd>Telescope git_status<CR>", "Open changed file" },
                              p = { "<cmd>Gitsigns preview_hunk<CR>", "Preview Hunk" },
                              q = { "<cmd>Gitsigns setqflist<CR>", "Quick fix" },
                              R = { "<cmd>Gitsigns reset_buffer<CR>", "Reset Buffer" },
                              r = { "<cmd>Gitsigns reset_hunk<CR>", "Reset Hunk" },
                              s = { "<cmd>Gitsigns stage_hunk<CR>", "Stage Hunk" },
                              S = { "<cmd>Gitsigns stage_buffer<CR>", "Stage Buffer" },
                              t = { "<cmd>Telescope git_status<CR>", "Stats" },
                              u = { "<cmd>Gitsigns undo_stage_hunk<CR>", "Undo Stage Hunk", },
                        },
                        ["H"] = { "<cmd>set hlsearch<CR>", "Highlight" },
                        ["h"] = { "<cmd>set nohlsearch<CR>", "No Highlight" },
                        l = {
                              name = "LSP",
                              a = { "<cmd>lua vim.lsp.buf.code_action()<CR>", "Code Action" },
                              d = { "<cmd>lua vim.diagnostic.open_float()<CR>", "Diagnostics"},
                              f = { "<cmd>lua vim.lsp.buf.format()<CR>", "Format" },
                              i = { "<cmd>LspInfo<CR>", "Info" },
                              I = { "<cmd>LspInstallInfo<CR>", "Install Info" },
                              j = { "<cmd>lua vim.diagnostic.goto_next()<CR>", "CodeLens Action" },
                              k = { "<cmd>lua vim.diagnostic.goto_prev()<CR>", "Quickfix" },
                              l = { "<cmd>lua vim.lsp.codelens.run()<CR>", "CodeLens Action" },
                              q = { "<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>", "Quickfix" },
                              r = { "<cmd>lua vim.lsp.buf.rename()<CR>", "Rename" },
                              s = { "<cmd>Telescope lsp_document_symbols<CR>", "Document Symbols" },
                              S = { "<cmd>Telescope lsp_dynamic_workspace_symbols<CR>", "Workspace Symbols" },
                              x = { "<cmd>PrettyXML<CR>", "Format XML"},
                        },
                        N = {
                              name = "Notifications",
                              a = { '<cmd>Noice all<CR>', "All" },
                              d = { '<cmd>Noice dismiss<CR>', "Dismiss all" },
                              h = { '<cmd>Noice history<CR>', "History" },
                              l = { '<cmd>Noice last<CR>', "Last" },
                        },
                        n = {
                              name = "Neorg",
                              c = { "<cmd>Neorg toggle-concealer<CR>", "Toggle Concealer"},
                              e = { "<cmd>Neorg index<CR>", "Enter Neorg default workspace"},
                              h = { "<cmd>Neorg inject-metadata<CR>", "Add metadata header"},
                              l = {
                                    name = "List",
                                    t = { "<localleader>lt<CR>", "Toggle lists"},
                                    i = { "<localleader>li", "Invert lists"},
                              },
                              m = {
                                    name = "Mode",
                                    t = { "<cmd>Neorg mode traverse-heading<CR>", "Navigate Headers"},
                                    n = { "<cmd>Neorg mode norg<CR>", "Navigate normally"},
                              },
                              t = { "<cmd>Neorg toc<CR>", "Open Table Of Contents (TOC)"},
                        },
                        ["o"] = { "<cmd>setlocal spell! spelllang=en_us<CR>", "Ortography" },
                        p = {
                              name = "Plugin",
                              b = { "<cmd>Lazy build<CR>", "Build" },
                              c = { "<cmd>Lazy check<CR>", "Check" },
                              C = { "<cmd>Lazy clean<CR>", "Clean" },
                              d = { "<cmd>Lazy debug<CR>", "Debug" },
                              h = { "<cmd>Lazy help<CR>", "Help" },
                              H = { "<cmd>Lazy home<CR>", "Home" },
                              i = { "<cmd>Lazy install<CR>", "Install" },
                              l = { "<cmd>Lazy log<CR>", "Log" },
                              p = { "<cmd>Lazy profile<CR>", "Profile" },
                              R = { "<cmd>Lazy clear<CR>", "Clear" },
                              r = { "<cmd>Lazy reload<CR>", "Reload" },
                              S = { "<cmd>Lazy show<CR>", "Show" },
                              s = { "<cmd>Lazy sync<CR>", "Sync" },
                        ["q"] = { "<cmd>q!<CR>", "Quit" },
                        s = {
                              name = "Search",
                              a = { "<cmd>Telescope autocommands<CR>", "Autocommands" },
                              b = { "<cmd>Telescope buffers sort_mru=true sort_lastused=true<CR>", "Show buffers" },
                              B = { "<cmd>Telescope git_branches<CR>", "Checkout branch" },
                              c = { "<cmd>Telescope command_history<CR>", "Command history" },
                              C = { "<cmd>Telescope commands<CR>", "Commands available" },
                              d = { "<cmd>Telescope diagnostics bufnr=0<CR>", "Document diagnostics" },
                              D = { "<cmd>Telescope diagnostics<CR>", "Workspace diagnostics" },
                              F = { "<cmd>Telescope current_buffer_fuzzy_find<CR>", "This FILE" },
                              f = { "<cmd>Telescope find_files<CR>", "Files" },
                              G = { "<cmd>Telescope git_files<CR>", "Git files" },
                              g = { "<cmd>Telescope grep_string<CR>", "Grep strings" },
                              h = { "<cmd>Telescope help_tags<CR>", "Find Help" },
                              H = { "<cmd>Telescope highlights<CR>", "Search Highlight Groups"},
                              k = { "<cmd>Telescope keymaps<CR>", "Keymaps" },
                              l = { "<cmd>Telescope live_grep<CR>", "Greps for lines" },
                              L = { "<cmd>Telescope Resume<CR>", "Last telescope search" },
                              M = { "<cmd>Telescope man_pages<CR>", "Man Pages" },
                              m = { "<cmd>Telescope marks<CR>", "Marks" },
                              o = { "<cmd>Telescope vim_options<CR>", "Options" },
                              r = { "<cmd>Telescope oldfiles<CR>", "Open Recent File" },
                              R = { "<cmd>Telescope registers<CR>", "Registers" },
                              s = { "<cmd>Telescope search_history<CR>", "Searches ran" },
                              t = { "<cmd>Telescope colorscheme<CR>", "Themes/Colorschemes" },
                        },
                        T = {
                              name = "Trouble",
                              T = { "<cmd>TroubleToggle<CR>", "Open Trouble" },
                              w = { "<cmd>TroubleToggle workspace_diagnostics<CR>", "Workspace diagnostics" },
                              d = { "<cmd>TroubleToggle document_diagnostics<CR>", "Document diagnostics" },
                              q = { "<cmd>TroubleToggle quickfix<CR>", "Quick fix" },
                              l = { "<cmd>TroubleToggle loclist<CR>", "Location Fix" },
                              r = { "<cmd>TroubleToggle lsp_references<CR>", "References" },
                        },
                        t = {
                              name = "Tab",
                              c = { "<cmd>tabclose<CR>", "Close" }, -- Close
                              d = { "<cmd>tabclose<CR>", "Close" }, -- Delete
                              k = { "<cmd>tabclose<CR>", "Close" }, -- Kill
                              n = { "<cmd>tabnext<CR>", "Next" },
                              N = { "<cmd>tabnew<CR>", "New empty file" },
                              l = { "<cmd>tabs<CR>", "List" },
                              p = { "<cmd>tabPrevious<CR>", "Previous" },
                        },
                        ["w"] = { "<cmd>w!<CR>", "Save" },
                        },
            },
      },
      config = function(_, opts)
            local wk = require("which-key")
            wk.setup(opts)
            wk.register(opts.nmappings)
      end,
}
