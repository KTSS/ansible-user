return {
      -- CMP - Engines for completing strings for you
      {
            "hrsh7th/nvim-cmp",
            version = false, -- last release is too old
            event = "InsertEnter",
            dependencies = {
                  "hrsh7th/cmp-nvim-lsp",
                  "hrsh7th/cmp-buffer",
                  "hrsh7th/cmp-path",
                  "saadparwaiz1/cmp_luasnip",
            },
            opts = function()
                  vim.api.nvim_set_hl(0, "CmpGhostText", { link = "Comment", default = true })
                  local cmp = require("cmp")
                  local defaults = require("cmp.config.default")()
                  local lsp_zero = require('lsp-zero')
                  local cmp_action = lsp_zero.cmp_action()
                  lsp_zero.extend_cmp()
                  return {
                        completion = {
                              completeopt = "menu,menuone,noinsert",
                        },
                        snippet = {
                              expand = function(args)
                                    require("luasnip").lsp_expand(args.body)
                              end,
                        },
                        mapping = cmp.mapping.preset.insert({
                              -- ctrl + n and +p already works and is builtin
                              --["<C-n>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
                              --["<C-p>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
		                      ["<C-j>"] = cmp.mapping.select_next_item(),
		                      ["<C-k>"] = cmp.mapping.select_prev_item(),
                              ['<C-f>'] = cmp_action.luasnip_jump_forward(),
                              ['<C-b>'] = cmp_action.luasnip_jump_backward(),
                              ['<C-u>'] = cmp.mapping.scroll_docs(-4),
                              ['<C-d>'] = cmp.mapping.scroll_docs(4),
                              ["<C-Space>"] = cmp.mapping.complete(),
                              ["<C-e>"] = cmp.mapping.abort(),
		                      ["<C-y>"] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
                              ["<CR>"] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
                              ["<S-CR>"] = cmp.mapping.confirm({
                                    behavior = cmp.ConfirmBehavior.Replace,
                                    select = true,
                              }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
                              ["<C-CR>"] = function(fallback)
                                  cmp.abort()
                                  fallback()
                              end,
                              ["<Tab>"] = cmp.mapping(function(fallback)
                                  if cmp.visible() then
                                      cmp.select_next_item()
                                  elseif luasnip.expandable() then
                                      luasnip.expand()
                                  elseif luasnip.expand_or_jumpable() then
                                      luasnip.expand_or_jump()
                                  elseif check_backspace() then
                                      fallback()
                                  else
                                      fallback()
                                  end
                                  end, {
                                      "i",
                                      "s",
                              }),
                              ["<S-Tab>"] = cmp.mapping(function(fallback)
                                  if cmp.visible() then
                                      cmp.select_prev_item()
                                  elseif luasnip.jumpable(-1) then
                                      luasnip.jump(-1)
                                  else
                                      fallback()
                                  end
                                  end, {
                                      "i",
                                      "s",
                              }),
                        }),
                        formatting = lsp_zero.cmp_format(),
                        sources = cmp.config.sources({
                              { name = "nvim_lsp" },
                              { name = "luasnip" },
                              { name = "path" },
                              { name = "buffer" },
                              { name = "neorg" },
                        }),
                        experimental = {
                              ghost_text = {
                                    hl_group = "CmpGhostText",
                              },
                        },
                        sorting = defaults.sorting,
                  }
            end,
            ---@param opts cmp.ConfigSchema
            config = function(_, opts)
                  for _, source in ipairs(opts.sources) do
                        source.group_index = source.group_index or 1
                  end
                  require("cmp").setup(opts)
            end,
      },

      -- LuaSnip: Snippets
      {
            "L3MON4D3/LuaSnip",
            dependencies = {
                  "rafamadriz/friendly-snippets",
                  config = function()
                        require("luasnip.loaders.from_vscode").lazy_load()
                  end,
            },
            opts = {
                  history = true,
                  delete_check_events = "TextChanged",
            },
            -- stylua: ignore
            keys = {
                  {
                        "<Tab>",
                        function()
                              return require("luasnip").jumpable(1) and "<Plug>luasnip-jump-next" or "<tab>"
                        end,
                        expr = true, silent = true, mode = "i",
                  },
                  { "<Tab>", function() require("luasnip").jump(1) end, mode = "s" },
                  { "<S-Tab>", function() require("luasnip").jump(-1) end, mode = { "i", "s" } },
            },
      },

      -- mini.surround: Help surrounding strings with other things
      {
            "echasnovski/mini.surround",
            keys = function(_, keys)
                  -- Populate the keys based on the user's options
                  local plugin = require("lazy.core.config").spec.plugins["mini.surround"]
                  local opts = require("lazy.core.plugin").values(plugin, "opts", false)
                  local mappings = {
                        { opts.mappings.add, desc = "Add surrounding", mode = { "n", "v" } },
                        { opts.mappings.delete, desc = "Delete surrounding" },
                        { opts.mappings.find, desc = "Find right surrounding" },
                        { opts.mappings.find_left, desc = "Find left surrounding" },
                        { opts.mappings.highlight, desc = "Highlight surrounding" },
                        { opts.mappings.replace, desc = "Replace surrounding" },
                        { opts.mappings.update_n_lines, desc = "Update `MiniSurround.config.n_lines`" },
                  }
                  mappings = vim.tbl_filter(function(m)
                        return m[1] and #m[1] > 0
                  end, mappings)
                  return vim.list_extend(mappings, keys)
            end,
            opts = {
                  mappings = {
                        add = "gsa", -- Add surrounding in Normal and Visual modes
                        delete = "gsd", -- Delete surrounding
                        find = "gsf", -- Find surrounding (to the right)
                        find_left = "gsF", -- Find surrounding (to the left)
                        highlight = "gsh", -- Highlight surrounding
                        replace = "gsr", -- Replace surrounding
                        update_n_lines = "gsn", -- Update `n_lines`
                  },
            },
      },

      -- Completion
      {
            "JoosepAlviste/nvim-ts-context-commentstring",
            lazy = true,
            opts = {
                  enable_autocmd = false,
            },
      },
}
