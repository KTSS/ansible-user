return {

      "nvim-treesitter/nvim-treesitter",
      version = false,
      build = ":TSUpdate",
      event = "VeryLazy",
      dependencies = {
            "nvim-treesitter/nvim-treesitter-textobjects",
            "JoosepAlviste/nvim-ts-context-commentstring",
            "p00f/nvim-ts-rainbow",
      },

      opts = {

            ensure_installed = {
                  "awk",
                  "bash",
                  "dockerfile",
                  "git_config",
                  "git_rebase",
                  "gitcommit",
                  "gitignore",
                  "ini",
                  "jq",
                  "json",
                  "lua",
                  "markdown",
                  "norg",
                  "regex",
                  "scheme",
                  "toml",
                  "vim",
                  "yaml"
            },

            sync_install = false,
            ignore_install = { "" }, -- List of parsers to ignore installing
            autopairs = { enable = true },
            indent = { enable = true, disable = { "yaml" } },

            highlight = {
                  enable = true, -- false will disable the whole extension
                  disable = { "" }, -- list of language that will be disabled
                  additional_vim_regex_highlighting = true,
            },

            rainbow = {
                  enable = true,
                  extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
                  max_file_lines = nil, -- Do not enable for files with more than n lines, int
            },

            context_commentstring = {
                  enable = true,
                  enable_autocmd = false,
            },
      },
}
