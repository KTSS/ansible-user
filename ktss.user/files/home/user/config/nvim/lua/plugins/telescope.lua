return {
      "nvim-telescope/telescope.nvim",
      cmd = "Telescope",
      version = false,
      dependencies = {
            {
                  "nvim-telescope/telescope-fzf-native.nvim",
                  build = "make",
                  enabled = vim.fn.executable("make") == 1,
            },
      },
      opts = function()
            local actions = require("telescope.actions")

            local open_with_trouble = function(...)
                  return require("trouble.providers.telescope").open_with_trouble(...)
            end
            local open_selected_with_trouble = function(...)
                  return require("trouble.providers.telescope").open_selected_with_trouble(...)
            end

            return {
                  defaults = {
                        prompt_prefix = " ",
                        selection_caret = " ",
                        -- open files in the first window that is an actual file.
                        -- use the current window if no other window is available.
                        get_selection_window = function()
                              local wins = vim.api.nvim_list_wins()
                              table.insert(wins, 1, vim.api.nvim_get_current_win())
                              for _, win in ipairs(wins) do
                                    local buf = vim.api.nvim_win_get_buf(win)
                                    if vim.bo[buf].buftype == "" then
                                          return win
                                    end
                              end
                              return 0
                        end,
                        mappings = {
                              i = {
                                    -- ["<a-i>"] = find_files_no_ignore,
                                    -- ["<a-h>"] = find_files_with_hidden,
                                    ["<C-Down>"] = actions.cycle_history_next,
                                    ["<C-Up>"] = actions.cycle_history_prev,
                                    ["<C-f>"] = actions.preview_scrolling_down,
                                    ["<C-b>"] = actions.preview_scrolling_up,
                              },
                              n = {
                                    ["q"] = actions.close,
                                    -- tip: on normal mode, press ? to get default shortcuts
                              },
                        },
                  },
            }
      end,
}
