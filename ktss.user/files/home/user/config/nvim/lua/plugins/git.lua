return {
    -- Git Signs, super fast git decorations implemented purely in Lua.
    {
        "lewis6991/gitsigns.nvim",
        opts = {
            signs = {
                add = { text = "▎" },
                change = { text = "▎" },
                delete = { text = "" },
                topdelete = { text = "" },
                changedelete = { text = "▎" },
                untracked = { text = "▎" },
            },
            on_attach = function(buffer)
                signcolumn = true, -- Toggle with `:Gitsigns toggle_signs`
                numhl = true, -- Toggle with `:Gitsigns toggle_numhl`
                linehl = true, -- Toggle with `:Gitsigns toggle_linehl`
                word_diff = true, -- Toggle with `:Gitsigns toggle_word_diff`
                watch_gitdir = {
                    interval = 10000,
                    follow_files = true,
                },
                attach_to_untracked = true,
                current_line_blame = true, -- Toggle with `:Gitsigns toggle_current_line_blame`
                current_line_blame_opts = {
                    virt_text = true,
                    virt_text_pos = "eol", -- 'eol' | 'overlay' | 'right_align'
                    delay = 2000,
                    ignore_whitespace = false,
                },
                current_line_blame_formatter_opts = { relative_time = false, },
                sign_priority = 6,
                update_debounce = 100,
                status_formatter = nil, -- Use default
                max_file_length = 40000,
                preview_config = {
                    -- Options passed to nvim_open_win
                    border = "single",
                    style = "minimal",
                    relative = "cursor",
                    row = 0,
                    col = 1,
                },
                yadm = { enable = false, },
                local gs = package.loaded.gitsigns
            },
      },

      -- NeoGit, a magit clone for neovim
      {
            "NeogitOrg/neogit",
            dependencies = {
                  "nvim-lua/plenary.nvim",         -- required
                  "nvim-telescope/telescope.nvim", -- optional
            },
            config = true
      },
}
