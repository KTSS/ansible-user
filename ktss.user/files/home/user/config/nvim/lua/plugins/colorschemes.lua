return {
      -- To set whichever colorscheme, basically use the following code
      -- lazy = false,
      -- priority = 1000, -- must be above 50, the default priority for plugins in lazy loading
      -- opts = {} -- the options you might want for the plugin
      -- config = function()
      --       vim.cmd([[ colorscheme <+colorscheme+> ]])
      -- end
      {
            "ellisonleao/gruvbox.nvim",
            "ishan9299/nvim-solarized-lua",
            "NTBBloodbath/doombox.nvim",
            "shaunsingh/nord.nvim",
      },
      {
            "folke/tokyonight.nvim",
            lazy = false,
            priority = 1000,
            opts = {},
            config = function()
                  vim.cmd([[ colorscheme tokyonight-night ]])
            end,
     },
     {
           "NTBBloodbath/doom-one.nvim",
           setup = function()
                 vim.g.doom_one_cursor_coloring = true -- Add color to cursor
                 vim.g.doom_one_terminal_colors = true -- Set :terminal colors
                 vim.g.doom_one_italic_comments = false -- Enable italic comments
                 vim.g.doom_one_enable_treesitter = true -- Enable TS support
                 vim.g.doom_one_diagnostics_text_color = true -- Color whole diagnostic text or only underline
                 vim.g.doom_one_transparent_background = true -- Enable transparent background

                 -- Pumblend transparency
                 vim.g.doom_one_pumblend_transparency = 69
                 vim.g.doom_one_pumblend_enable = true

                 -- Plugins integration
                 vim.g.doom_one_plugin_barbar = false
                 vim.g.doom_one_plugin_dashboard = false
                 vim.g.doom_one_plugin_indent_blankline = false
                 vim.g.doom_one_plugin_nvim_tree = false
                 vim.g.doom_one_plugin_vim_illuminate = false
                 vim.g.doom_one_plugin_startify = false

                 vim.g.doom_one_plugin_neogit = true
                 vim.g.doom_one_plugin_lspsaga = true
                 vim.g.doom_one_plugin_neorg = true
                 vim.g.doom_one_plugin_telescope = true
                 vim.g.doom_one_plugin_whichkey = true
           end,
     },
}
