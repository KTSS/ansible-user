-- *.tex

vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'v', '<leader>', '<ESC>`<i\\{<ESC>`>2la}<ESC>?\\\\{<CR>a' ) end, })

-- Word count:
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',wc', ':w !detex \\| wc -w<CR>' ) end, })

-- Basic formatting
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',bf', '\\textbf{}<Space><++><Esc>T{i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',em', '\\emph{}<Space><++><Esc>T{i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',it', '\\textit{}<Space><++><Esc>T{i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',tt', '\\texttt{}<Space><++><Esc>T{i' ) end, })

-- Code snippets
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',a', '\\href{}{<++>}<Space><++><Esc>2T{i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',bt', '{\\blindtext}' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',can', '\\cand{}<Tab><++><Esc>T{i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',ce', '\\begin{center}<CR><CR>\\end{center}<Esc>ki<Space><Space>' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',chap', '\\chapter{}<CR><CR><++><Esc>2kf}i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',col', '\\begin{columns}[T]<CR>\\begin{column}{.5\\textwidth}<CR><CR>\\end{column}<CR>\\begin{column}{.5\\textwidth}<CR><++><CR>\\end{column}<CR>\\end{columns}<Esc>5kA' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',con', '\\const{}<Tab><++><Esc>T{i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',cp', '\\parencite{}<++><Esc>T{i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',ct', '\\textcite{}<++><Esc>T{i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',da', '\\documentclass{article}<CR><CR>\\author{}<CR>\\title{<++>}<CR>\\date{<++>}<CR><CR>\\usepackage[lmargin=3cm,rmargin=2cm,tmargin=3cm,bmargin=2cm]{geometry}<CR><CR>\\begin{document}<CR>\\maketitle<CR><CR><++><CR><CR>\\end{document}<Esc>11k$i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',exe', '\\begin{exe}<CR>\\ex<Space><CR>\\end{exe}<CR><CR><++><Esc>3kA' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',fi', '\\begin{fitch}<CR><CR>\\end{fitch}<CR><CR><++><Esc>3kA' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',fr', '\\begin{frame}<CR>\\frametitle{}<CR><CR><++><CR><CR>\\end{frame}<CR><CR><++><Esc>6kf}i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',glos', '{\\gll<Space><++><Space>\\\\<CR><++><Space>\\\\<CR>\\trans{``<++>\'\'}}<Esc>2k2bcw' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',img', '\\begin{figure}[h!]<CR>\\centering<CR>\\includegraphics[width=0.5\\textwidth]{}<CR>\\caption{<++>}<CR>\\end{figure}<CR><CR><++><Esc>4k$i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',lab', '\\label{}<Esc>i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',ld', '\\ldots ' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',nl', '\\newline' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',np', '\\newpage<CR><CR>i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',nu', '$\\varnothing$' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',pkg', '\\usepackage{}<Esc>i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',ref', '\\ref{}<Space><++><Esc>T{i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',rn', '(\\ref{})<++><Esc>F}i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',sc', '\\textsc{}<Space><++><Esc>T{i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',st', '<Esc>F{i*<Esc>f}i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',v', '\\vio{}<Tab><++><Esc>T{i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',x', '\\begin{xlist}<CR>\\ex<Space><CR>\\end{xlist}<Esc>kA<Space>' ) end, })

-- -- Diacritics/accents
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'á', '\\\'{a}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'à', '\\`{a}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'ã', '\\~{a}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'â', '\\^{a}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'ç', '\\c{c}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'é', '\\\'{e}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'ê', '\\^{e}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'í', '\\\'{i}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'ó', '\\\'{o}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'õ', '\\~{o}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'ô', '\\^{o}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'ú', '\\\'{u}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'Á', '\\\'{A}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'À', '\\`{A}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'Ã', '\\~{A}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'Â', '\\^{A}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'Ç', '\\c{C}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'É', '\\\'{E}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'Ê', '\\^{E}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'Í', '\\\'{I}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'Ó', '\\\'{O}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'Õ', '\\~{O}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'Ô', '\\^{O}' ) end, })
-- vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', 'Ú', '\\\'{U}' ) end, })

-- Lists
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',li', '<CR>\\item<Space>' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',ol', '\\begin{enumerate}<CR><CR>\\end{enumerate}<CR><CR><++><Esc>3kA\\item<Space>' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',ul', '\\begin{itemize}<CR><CR>\\end{itemize}<CR><CR><++><Esc>3kA\\item<Space>' ) end, })

-- Sections
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',sec', '\\section{}<CR><CR><++><Esc>2kf}i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',ssec', '\\subsection{}<CR><CR><++><Esc>2kf}i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',sssec', '\\subsubsection{}<CR><CR><++><Esc>2kf}i' ) end, })

-- Single and Double quotes
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',sq', '`\'<Space><++><Esc>5hi' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',dq', '``\'\'<Space><++><Esc>6hi' ) end, })

-- Tables
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',te', '<Esc>$i\\\\ \\hline<CR>' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',lt', '\\begin{longtable}[h]{}<CR>\\caption{<++>}<CR>\\label{<++>}<Space>\\\\<Space>\\hline<CR><Space><Space><Space><Space>\\textbf{<++>}<Space>&<Space>\\textbf{<++>}<Space>\\\\<Space>\\hline<CR><++><Space>&<Space><++><Space>\\\\<Space>\\hline<CR>\\end{longtable}<Esc>5k$i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',ot', '\\begin{tableau}<CR>\\inp{<++>}<Tab>\\const{<++>}<Tab><++><CR><++><CR>\\end{tableau}<CR><CR><++><Esc>5kA{}<Esc>i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'tex', callback = function() vim.keymap.set( 'i', ',tab', '\\begin{tabular}<CR><++><CR>\\end{tabular}<CR><CR><++><Esc>4kA{}<Esc>i' ) end, })
