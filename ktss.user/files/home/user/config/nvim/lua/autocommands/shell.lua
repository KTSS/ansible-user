-- *.sh
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'sh', callback = function() vim.keymap.set( 'i', ',bash', '#!/bin/bash<CR><CR>' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'sh', callback = function() vim.keymap.set( 'i', ',sh', '#!/bin/sh<CR><CR>' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'sh', callback = function() vim.keymap.set( 'i', ',zsh', '#!/usr/bin/env zsh<CR><CR>' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'sh', callback = function() vim.keymap.set( 'i', ',fu', '()<Space>{<CR><Tab><++><CR>}<CR><CR><++><Esc>?()<CR>' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'sh', callback = function() vim.keymap.set( 'i', ',for', 'for<Space><Space>in<Space><++><Space><++><CR>do<CR><++><CR>done<ESC>3k0f<Space>a' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'sh', callback = function() vim.keymap.set( 'i', ',if', 'if<Space>[<Space>];<Space>then<CR><++><CR>fi<CR><CR><++><Esc>?];<CR>hi<Space>' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'sh', callback = function() vim.keymap.set( 'i', ',elif', 'elif<Space>[<Space>];<Space>then<CR><++><CR><Esc>?];<CR>hi<Space>' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'sh', callback = function() vim.keymap.set( 'i', ',switch', 'case<Space>""<Space>in<CR><++>)<Space><++><Space>;;<CR><++><CR>esac<CR><CR><++><Esc>?"<CR>i' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'sh', callback = function() vim.keymap.set( 'i', ',case', ')<Space><++><Space>;;<CR><++><Esc>?)<CR>i' ) end, })
