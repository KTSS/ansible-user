-- *.norg

-- Header
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('n' , '<leader>he' , '<cmd>Neorg inject-metadata<CR>' , {buffer=0}) end, })

-- Shortcuts

vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('n' ,  '<BS>' ,  ':w<CR>:bdelete<CR>' , {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',fi'  ,  '{}[]<Esc>F{a'       , {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',no'  ,  '{::}<Esc>F:i'       , {buffer=0})  end,  })

--GTD

vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('n' ,  '<leader>ngc' ,  '<cmd>Neorg gtd capture<CR>',  {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('n' ,  '<leader>nge' ,  '<cmd>Neorg gtd edit<CR>',     {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('n' ,  '<leader>ngv' ,  '<cmd>Neorg gtd views<CR>',    {buffer=0})  end,  })

-- Lists

---- Unordered
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',1u' ,  '- ',       {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',2u' ,  '-- ',      {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',3u' ,  '--- ',     {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',4u' ,  '---- ',    {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',5u' ,  '----- ',   {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',6u' ,  '------ ',  {buffer=0})  end,  })

---- Ordered
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',1o' ,  '~ ',       {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',2o' ,  '~~ ',      {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',3o' ,  '~~~ ',     {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',4o' ,  '~~~~ ',    {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',5o' ,  '~~~~~ ',   {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',6o' ,  '~~~~~~ ',  {buffer=0})  end,  })

---- Headers
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',1h' ,  '* ',       {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',2h' ,  '** ',      {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',3h' ,  '*** ',     {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',4h' ,  '**** ',    {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',5h' ,  '***** ',   {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',6h' ,  '****** ',  {buffer=0})  end,  })

---- Checkboxes
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',1c' ,  '- ( ) ',       {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',2c' ,  '-- ( ) ',      {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',3c' ,  '--- ( ) ',     {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',4c' ,  '---- ( ) ',    {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',5c' ,  '----- ( ) ',   {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',6c' ,  '------ ( ) ',  {buffer=0})  end,  })


-- Other snippets

vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',cd' ,  '@code<CR>@end<Esc>O    <++><Esc>kA ',  {buffer=0})  end,  })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'norg', callback = function() vim.keymap.set('i' ,  ',tb' ,  '@table<CR>@end<Esc>O    <++><Esc>kA',  {buffer=0})  end,  })
