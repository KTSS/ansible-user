-- Vertically center document when entering insert mode
    vim.api.nvim_create_autocmd( "InsertEnter", { pattern = "*", callback = function() vim.api.nvim_command( "normal zz" ) end })

-- Runs a script that cleans out tex build files whenever I close out of a .tex file.
    vim.api.nvim_create_autocmd( "VimLeave", { pattern  = "*tex", callback = function() vim.api.nvim_command( "! texclear %" ) end })

-- Ensure files are read as what I want (vimwiki is controlled in its own vim
-- file):
    vim.api.nvim_create_autocmd({"BufRead", "BufNewFile"}, { pattern  = { "*.md","*.markdown","*.rmd","/tmp/calcurse*","~/.calcurse/notes/*"}, callback = function() vim.opt.filetype = "markdown" end, })
    vim.api.nvim_create_autocmd({"BufRead", "BufNewFile"}, { pattern  = {"*.ms","*.me","*.mom","*.man"}, callback = function() vim.opt.filetype = "groff" end, })
    vim.api.nvim_create_autocmd({"BufRead", "BufNewFile"}, { pattern  = {"*.tex"}, callback = function() vim.opt.filetype = "tex" end, })
    vim.api.nvim_create_autocmd({"BufRead", "BufNewFile"}, { pattern  = {"*.wiki"}, callback = function() vim.opt.filetype = "wiki" end, })

-- Automatically deletes all trailing whitespace and newlines at end of file on save.
    vim.api.nvim_create_autocmd( "BufWritePre", { pattern  = "*", callback = function() vim.api.nvim_command( "%s@\\n\\+\\%$@@e" ) end })
    vim.api.nvim_create_autocmd( "BufWritePre", { pattern  = "*", callback = function() vim.api.nvim_command( "%s@\\s\\+$@@e" ) end })
    -- vim.api.nvim_create_autocmd( "BufWritePre", { pattern  = { "*.c" , "*.h" }, callback = function() vim.api.nvim_command( "%s@\\%$@\\r@e" ) end })

-- Attach Ansible language server on yaml files
    vim.api.nvim_create_autocmd({"BufRead", "BufNewFile"}, { pattern = { "*.yml"}, callback = function() vim.cmd([[LspStart ansiblels]]) end })

-- When shortcut files are updated, renew shell configs with new material:
    vim.api.nvim_create_autocmd( "BufWritePost", { pattern  = { "bm-file","bm-dirs" }, callback = function() vim.api.nvim_command( "! shortcuts" ) end })

-- Run xrdb whenever Xdefaults or Xresources are updated.
    vim.api.nvim_create_autocmd( "BufWritePost", { pattern  = { "*Xresources" , "*Xdefaults" , "*xresources" , "*xdefaults" }, callback = function() vim.api.nvim_command( "! xrdb %" ) end })

-- Automatically highlights on yank
    vim.api.nvim_create_autocmd( "TextYankPost", { callback = function() vim.highlight.on_yank({ higroup = "Visual", timeout = 50}) end })

-- Autocompile dwmblocks when file is changed
    --vim.api.nvim_create_autocmd( "BufWritePost", { pattern  = { "~/.local/src/dwmblocks/config.h" }, command = function() vim.api.nvim_command( "! cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks &" ) end, })
