-- *.xml
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'xml', callback = function() vim.keymap.set( 'i', ',e', '<item><Enter><title><++></title><Enter><guid<space>isPermaLink="false"><++></guid><Enter><pubDate><Esc>:put<Space>=strftime(\'%a, %d %b %Y %H:%M:%S %z\')<Enter>kJA</pubDate><Enter><link><++></link><Enter><description><![CDATA[<++>]]></description><Enter></item><Esc>?<title><enter>cit' ) end, })
vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'xml', callback = function() vim.keymap.set( 'i', ',a', '<a href="<++>"><++></a><++><Esc>F"ci"' ) end, })
