-- *.wiki
-- Loads the header

    -- appends template to new vimwiki files
       -- vim.api.nvim_create_autocmd( 'BufNewFile', { pattern  = 'wiki', callback = function() vim.cmd [[ source ~/.config/nvim/base/wiki-header.txt ]] end, })
       -- vim.api.nvim_create_autocmd( 'BufNewFile', { pattern  = 'wiki', callback = function() vim.cmd [[ execute "1," . 2 . "g/#title:.*/s@@#title:  " .expand("%") ]] end, })-- adds file name to title
       -- vim.api.nvim_create_autocmd( 'BufNewFile', { pattern  = 'wiki', callback = function() vim.cmd [[ execute "1," . 2 . "g/#date:.*/s@@#date:   " .strftime("%Y-%m-%d %R:%S") ]] end, })-- adds the date for the creation date as the date

-- Normal vimwiki editing

    --Links and checkboxes
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',li', '[[]]<Esc>hi'               ) end, }) -- Link
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',cb', '-<Space>[<Space>]<Space>'  ) end, }) -- Checkbox

    -- Headers (For Archwiki, don't use header level 1)
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',h1', '=<Space>=<CR><CR><++><Esc>2k0a<Space>'             ) end, }) -- Self explanatory
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',h2', '==<Space>==<CR><CR><++><Esc>2k0ea<Space>'          ) end, }) -- Self explanatory
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',h3', '===<Space>===<CR><CR><++><Esc>2k0ea<Space>'        ) end, }) -- Self explanatory
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',h4', '====<Space>====<CR><CR><++><Esc>2k0ea<Space>'      ) end, }) -- Self explanatory
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',h5', '=====<Space>=====<CR><CR><++><Esc>2k0ea<Space>'    ) end, }) -- Self explanatory
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',h6', '======<Space>======<CR><CR><++><Esc>2k0ea<Space>'  ) end, }) -- Self explanatory

-- Archwiki Editing

    -- AUR packages, packages and Manpages
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',au', '{{AUR|}}<Space><++><Esc>6hi'        ) end, }) -- AUR
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',ma', '{{man||<++>}}<Space><++><Esc>11hi'  ) end, }) -- Manpage
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',pk', '{{Pkg|}}<Space><++><Esc>6hi'        ) end, }) -- Package

    -- Code blocks
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',ic', '{{ic|1=}}<Space><++><Esc>T=i'                     ) end, }) -- Inline
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',hc', '{{hc||1=<CR><++><CR>}}<CR><CR><++><Esc>4k0f12ha'  ) end, }) -- With header
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',bc', '{{bc|1=<CR><CR>}}<CR><CR><++><Esc>3k0i'           ) end, }) -- Without header


   -- Notes
         vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',ne', '{{Note|}}<CR><CR><++><Esc>2kf|a'      ) end, })
         vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',np', '{{Nota|}}<CR><CR><++><Esc>2kf|a'      ) end, })

   -- Tips
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',te', '{{Tip|}}<CR><CR><++><Esc>2kf|a'   ) end, })
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',tp', '{{Dica|}}<CR><CR><++><Esc>2kf|a'  ) end, })

   -- Warnings
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',we', '{{Warning|}}<CR><CR><++><Esc>2kf|a'  ) end, })
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',wp', '{{Atenção|}}<CR><CR><++><Esc>2kf|a'  ) end, })

   -- Basic formatting
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',ib', "''''''''''<Space><++><Esc>10ha"  ) end, })
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',bd', "''''''<Space><++><Esc>8ha"       ) end, })
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',it', "''''<Space><++><Esc>7ha"         ) end, })

   -- HTML
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',hq', '<q></q><Space><++><Esc>8hi'       ) end, })
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',hu', '<u></u><Space><++><Esc>8hi'       ) end, })
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',ht', '<s></s><Space><++><Esc>8hi'       ) end, })
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',hb', '<sub></sub><Space><++><Esc>10hi'  ) end, })
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',hp', '<sup></sup><Space><++><Esc>10hi'  ) end, })

   -- Links
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',lp', '[[]]<Space><++><Esc>6hi'        ) end, }) -- To another page
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',lt', '[[|<++>]]<Space><++><Esc>11hi'  ) end, }) -- To another page (Choosing what to display)
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',ls', '[[#<++>]]<Space><++><Esc>10hi'  ) end, }) -- To section on another page
        vim.api.nvim_create_autocmd( 'Filetype', { pattern = 'wiki', callback = function() vim.keymap.set( 'i', ',se', '[[#]]<Space><++><Esc>6hi'       ) end, }) -- To section on same page
