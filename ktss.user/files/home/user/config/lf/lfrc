# Basic vars
set autoquit
set cleaner '~/.config/lf/cleaner'
set drawbox
set hiddenfiles ".*:*.aux:*.log:*.bbl:*.bcf:*.blg:*.run.xml"
set icons
set ifs "\n"
set info size:time
set number
set period 1
set previewer '~/.config/lf/scope'
set relativenumber
set scrolloff 10
set shellopts '-eu'

# cmds/functions
cmd open ${{
    case $(file --mime-type "$(readlink -f $f)" -b) in
	application/vnd.openxmlformats-officedocument.spreadsheetml.sheet) localc $fx ;;
	image/vnd.djvu|application/pdf|application/octet-stream|application/postscript) setsid -f zathura $fx >/dev/null 2>&1 ;;
        text/*|application/json|inode/x-empty) $EDITOR $fx;;
	image/x-xcf) setsid -f gimp $f >/dev/null 2>&1 ;;
	image/svg+xml) display -- $f ;;
	image/*) rotdir $f | grep -i "\.\(png\|jpg\|jpeg\|gif\|webp\|tif\|ico\)\(_large\)*$" |
		setsid -f sxiv -baio 2>/dev/null | while read -r file; do
			[ -z "$file" ] && continue
			lf -remote "send select \"$file\""
			lf -remote "send toggle"
		done &
		;;
	audio/*) mpv --audio-display=no $f ;;
	video/*) setsid -f mpv $f -quiet >/dev/null 2>&1 ;;
	application/pdf|application/vnd*|application/epub*) setsid -f zathura $fx >/dev/null 2>&1 ;;
	application/pgp-encrypted) $EDITOR $fx ;;
	application/vnd.openxmlformats-officedocument.wordprocessingml.document|application/vnd.oasis.opendocument.text) setsid -f lowriter $fx >/dev/null 2>&1 ;;
	application/vnd.openxmlformats-officedocument.spreadsheetml.sheet|application/octet-stream|application/vnd.oasis.opendocument.spreadsheet|application/vnd.oasis.opendocument.spreadsheet-template) setsid -f localc $fx >/dev/null 2>&1 ;;
	application/vnd.openxmlformats-officedocument.presentationml.presentation|application/vnd.oasis.opendocument.presentation-template|application/vnd.oasis.opendocument.presentation|application/vnd.ms-powerpoint) setsid -f loimpress $fx >/dev/null 2>&1 ;;
	application/vnd.oasis.opendocument.graphics|application/vnd.oasis.opendocument.graphics-template) setsid -f lodraw $fx >/dev/null 2>&1 ;;
	application/vnd.oasis.opendocument.formula) setsid -f lomath $fx >/dev/null 2>&1 ;;
	application/vnd.oasis.opendocument.database) setsid -f lobase $fx >/dev/null 2>&1 ;;
        *) for f in $fx; do setsid -f $OPENER $f >/dev/null 2>&1; done;;
    esac
}}

cmd mkdir $mkdir -p "$(echo $* | tr ' ' '\ ')"

cmd delete ${{
    clear; tput cup $(($(tput lines)/3)); tput bold
    set -f
    printf "%s\n\t" "$fx"
    printf "delete?[y/N]"
    read ans
    [ $ans = "y" ] && rm -rf -- $fx
}}

cmd moveto ${{
    clear; tput cup $(($(tput lines)/3)); tput bold
    set -f
    clear; echo "Move to where?"
    dest="$(sed -e 's/#.*//' -e '/^$/d' -e 's/^\S*\s*//' ${XDG_CONFIG_HOME:-$HOME/.config}/shell/bm-dirs | sk | sed 's@~@$HOME@')" &&
    for x in $fx; do
        eval mv -iv \"$x\" \"$dest\"
    done &&
    notify-send " File(s) moved." "File(s) moved to $dest."
}}

cmd copyto ${{
    clear; tput cup $(($(tput lines)/3)); tput bold
    set -f
    clear; echo "Copy to where?"
    dest="$(sed -e 's/#.*//' -e '/^$/d' -e 's/^\S*\s*//' ${XDG_CONFIG_HOME:-$HOME/.config}/shell/bm-dirs | sk | sed 's@~@$HOME@')" &&
    for x in $fx; do
        eval cp -ivr \"$x\" \"$dest\"
    done &&
    notify-send " File(s) copied." "File(s) copies to $dest."
}}

cmd bulkrename $vidir

cmd setbg "$1"

cmd dragon %dragon-drop -a -x "$fx"
cmd cpdragon %cpdragon
cmd mvdragon %mvdragon

# Unarchive bindings
cmd unarchive ${{
  case "$f" in
        *.tar)      tar    -xvf   "$f"  ;;
        *.tar.bz2)  tar    -xjvf  "$f"  ;;
        *.tar.gz)   tar    -xzvf  "$f"  ;;
        *.tar.xz)   tar    -xJvf  "$f"  ;;
        *.tar.zst)  tar    -xvf   "$f"  ;;
        *.zip)      unzip         "$f"  ;;
      *) echo "Unsupported format"      ;;
  esac
}}

# Archive bindings
cmd tar    %tar cvf                                                    "$f.tar"      "$f"
cmd tarbz2 %tar cjvf                                                   "$f.tar.bz2"  "$f"
cmd targz  %tar cvzf                                                   "$f.tar.gz"   "$f"
cmd tarxz  %tar -cvf - "$f" | xz -9e -c -v                           > "$f.tar.xz"
cmd tarzst %tar -cvf - "$f" | zstd  --compress  --ultra  --22  --vvv > "$f.tar.zst"
cmd zip    %zip -r                                                     "$f.zip"      "$f"

# General Bindings
map <c-e>   down
map <c-f>   $lf -remote "send $id select '$(sk)'"
map <c-h>   set hidden!
map <c-n>   push     :mkdir<space>
map <c-r>   reload
map <c-y>   up
map <enter> shell
map a       push        A<a-b>            # After extention
map A       rename                        # At the very end
map b       bulkrename
map B       $setbg          "$f"
map C       copyto
map c       push        A<c-u>            # New rename
map D       delete
map E       unarchive
map gg      top
map i       push        A<a-b><a-b><a-f>  # Before extention
map I       push        A<c-a>            # At the very beginning
map J       $lf -remote "send $id cd $(sed -e 's/#.*//' -e '/^$/d' -e 's/^\S*\s*//' ${XDG_CONFIG_HOME:-$HOME/.config}/shell/bm-dirs | sk )"
map M       moveto
map O       $mimeopen --ask "$f"
map o       &mimeopen       "$f"
map V       push :$nvim<space>
map X       !$f
map x       $$f
map Y $printf "%s" "$fx" | xclip -selection clipboard
map t

# Archiving bindings
map ta unarchive # "t" for tar
map tb tarbz2    # "t" for tar
map tg targz     # "t" for tar
map ts tarzst    # "t" for tar
map tt tar       # "t" for tar
map tx tarxz     # "t" for tar
map tz zip       # "t" for tar

#Dragon bindigns
map tc cpdragon # German ... treiben = to push
map tl dlfile   # German ... treiben = to push
map tm mvdragon # German ... treiben = to push
map tr dragon   # German ... treiben = to push

# Source Bookmarks
source "~/.config/lf/shortcutrc"
